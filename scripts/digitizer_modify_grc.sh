#!/bin/sh

# exit on any error
set -e

SCRIPT IS WIP !!!!

function help {
    echo ""
    echo "Usage: Open grc file for Digitizer FEC <FEC_NAME>"
    echo ""
    echo "This script will download the grc file of a Digitizer FEC and open it with gnuradio-companion"
    echo ""
    echo "-v verbose"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -gt 2 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi
if [ $# -lt 1 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi

FEC=$1
VERBOSE=FALSE
if [ ! -z "$2" ]; then
        VERBOSE=TRUE
fi

NOMEN=$(pdex -a $FEC | grep NOMEN | awk '{print $3}')

# If there are multiple devices, grap the first non global
if [ -z "$NOMEN" ]; then
        NOMEN=$(pdex -a $FEC | grep -v 'global' | grep -m 1 '|--' |  awk '{print $3}')
fi

#TRy int
if [ -z "$NOMEN" ]; then
        NOMEN=$(idex -a $FEC | grep NOMEN | awk '{print $3}')
fi

# If there are multiple devices, grap the first non global
if [ -z "$NOMEN" ]; then
        NOMEN=$(idex -a $FEC | grep -v 'global' | grep -m 1 '|--' |  awk '{print $3}')
fi

if [ -z "$NOMEN" ]; then
        echo "FEC not found error for FEC: $FEC" >&2
        exit 1
fi

GRC=$(pdex -a $NOMEN FlowGraphConfig | grep '| status' | awk '{print $9}')

#TRy int
if [ -z "$STATUS" ]; then
        STATUS=$(idex -a $NOMEN Status | grep '| status' | awk '{print $9}')
fi

if [ -z "$STATUS" ]; then
        echo "Failed to get status for FEC: $FEC" >&2
        exit 1
fi

if [ "$VERBOSE" = "TRUE" ]; then
  echo "Status of '$NOMEN' : $STATUS"
fi

if [ "$STATUS" = "OK" ]; then
  exit 0
else
  exit 1
fi