This folder contains optization configs per FEC types.

E.g. the FEC-type "supermicro" runs gnuradio-modules which heavily use
vector-calculations via libvolk and fouier transformations via libfftw3.

The performance of these libraries can be speed up by the factor ~10 when
some tests are run to check the optimal CPU performance profile for different
possible calculation scenarios.

The result of these tests (which can take hours) is stored in a config file
which can be used on every identical FEC.


##############  Generation of wisdom file (to be executed on frontend) ############## 
fftwf-wisdom -v -c -o wisdom 

############## Build instructions for libfftwf (we need the float version, not the double version) ##########################################
- Download and unpack tatrball from http://fftw.org/download.html
# mkdir -p /home/bel/schwinn/lnx/Software/fftw
# /configure --prefix=/home/bel/schwinn/lnx/Software/fftw --enable-shared --enable-float --enable-threads --enable-sse --enable-sse2 --enable-avx --enable-avx2 "CC=gcc -march=native"
# make -j8
# make install