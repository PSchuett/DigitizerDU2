#!/bin/sh

function help {
    echo ""
    echo "Usage: temp_install_gr_on_FEC.sh <FEC-Name>"
    echo ""
    echo "This script will upload the gr-flowgraph and gr_digitizer library to the specified FEC (folder DigitizerDU)"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied. Check the help:
    help
fi

if [ $# -gt 1 ]
  then
    echo "Error: Wrong number of arguments supplied. Check the help:
    help
fi

SCRIPT=$(readlink -f "$0")
PROJECT_PATH=$(dirname "$(dirname "$SCRIPT")")

FEC=$1
FEC_FOLDER=/common/export/fesa/local/$FEC/DigitizerDU2/

cp -v ${PROJECT_PATH}/../gr-digitizers/build/lib/libgnuradio-digitizers-*.so.0.0.0 $FEC_FOLDER
echo "... copied gr-digitizer library to FEC"
cp -v ${PROJECT_PATH}/../gr-flowgraph/build/lib/libgnuradio-flowgraph-*.so.0.0.0 $FEC_FOLDER
echo "... copied gr-flowgraph library to FEC"

