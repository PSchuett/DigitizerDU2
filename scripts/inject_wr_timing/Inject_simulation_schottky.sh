#!/bin/sh

GID=300 

#####################
# Sample data measured from dal005
#####################
#2021-03-23 15:35:30.681 FAIR.SELECTOR.C=6:T=300:S=4:P=28: CMD_BEAM_INJECTION
#2021-03-23 15:35:30.681 FAIR.SELECTOR.C=6:T=300:S=4:P=30: CMD_START_ENERGY_RAMP
#2021-03-23 15:35:30.681 FAIR.SELECTOR.C=6:T=300:S=4:P=32: CMD_BEAM_EXTRACTION
#2021-03-23 15:35:30.681 FAIR.SELECTOR.C=6:T=300:S=4:P=30: CMD_START_ENERGY_RAMP (kommt manchmal nochmal)

#- 1sekunde lang nix

#16:35:38:818
#16:35:38:870 + 52ms
#16:35:39:550 + 680ms
#
#- 1sekunde lang nix
#
#16:38:40:622
#16:38:41:007 + 385ms
#16:38:41:429 + 422ms
#16:38:41:513 + 84ms
#
#- 1sekunde lang nix
#
#...
#
#-----------------------------------
#
#62,5MS/s 
#1s     --> 62,5MS
#19.2ms --> 1,2MS


help()
{
    echo "$0 <ACC Group>"
    echo ""
    echo "This script will periodically simulate timing for the specified accelerator group. If no group was specified, '300' (SIS18 Ring) will be used."
    echo ""
    echo "Group ID: 210 CRYRING RING"
    echo "Group ID: 300 SIS18 Ring"
    echo "Group ID: 340 ESR Ring"
    echo ""
    exit 1;
}

if [ $# -eq 1 ]
  then
    GID=$1
fi
if [ $# -gt 1 ]
  then
    echo "Error: Wrong number arguments supplied. First and only argument has to be GID"
    help
    exit 1
fi
if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi
echo "Used accelerator group: $GID"

# Arguments: GroupID, EventID, SequenceID, ProcessID, ChainID, Flags
inject()
{
    id=0
    mask=0
    param=0
    
    #FormatID
    id=$((0x1<<60))
    mask=$((0xf<<60))
    
    #GroupID
    if [ "$#" -gt 0 ]
    then
      id=$(( (id) |  ($1<<48)  ))
      mask=$(( mask | (0xfff<<48) ))
    fi
    #EventID
    if [ "$#" -gt 1 ]
    then
      id=$(( (id) | ($2<<36)  ))
      mask=$(( mask | (0xfff<<36) ))
    fi
    #SequenceID
    if [ "$#" -gt 2 ]
    then
      id=$(( (id) | ($3<<20)  ))
      mask=$(( mask | (0xfff<<20) ))
    fi
    #BeamProcessID
    if [ "$#" -gt 3 ]
    then
      id=$(( (id) | ($4<<6)  ))
      mask=$(( mask | (0x3fff<<6) ))
    fi
    
    if [ "$#" -gt 4 ]
    then
      param=$(( (param) | ($5<<42)  ))
    fi
    
    #Flags Go Here
    if [ "$#" -gt 5 ]
    then
      id=$(( (id) | ($6<<32)  ))
    fi
    
    saft-ctl dummy -f inject $id $param 5000000
}

# Argumetns:
#  - $1: IO name
#
configure_io()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl tr0 -n $1 -x

  saft-io-ctl tr0 -n $1 -o1 -d0

  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 1
  saft-io-ctl tr0 -n $1 -u -c  0x1023000000000     0xfffffff000000000 3500000 0 0

  saft-io-ctl tr0 -i
  saft-io-ctl tr0 -l
  echo ""x
}

# Argumetns:
#  - $1: IO name
#
configure_io_timing()
{
  echo ""

  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "configuring $1, make sure to restart FESA class afterwards (output conditions will be deleted)"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

  echo ""

  saft-io-ctl baseboard -n $1 -o1 -d0

  # TIME_SYNC
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x107B000000000     0xfffffff000000000 2500000 0 0

  # INJECT
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1023000000000     0xfffffff000000000 2500000 0 0

  # USER 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1118000000000     0xfffffff000000000 2500000 0 0

  # USER 2
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x1119000000000     0xfffffff000000000 2500000 0 0

  # USER 3
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 0000000 0 1
  saft-io-ctl baseboard -n $1 -u -c  0x111A000000000     0xfffffff000000000 2500000 0 0

  saft-io-ctl baseboard -i
  saft-io-ctl baseboard -l
  echo ""
}

show_config()
{
  echo ""
  saft-io-ctl tr0 -l
  echo ""
}

# Executes a sequence (takes ~1sec with current time settings)
# Argumetns:
#  - $1: sequnce number (should be a single digit)
#

execute_sequnce3()
{
  echo "executing sequnce" $1 `date`
  
  # CMD_BEAM_INJECTION#283
  inject $GID 283 $1 4
  usleep 52000

  # CMD_START_ENERGY_RAMP#285
  inject $GID 285 $1 4 0x8
  usleep 680000
  
  # CMD_BEAM_EXTRACTION#284
  inject $GID 284 $1 4
  usleep 1000000
  
  echo "end of sequnce" $1 `date`
}

execute_sequnce7()
{
  echo "executing sequnce" $1 `date`
  
  # CMD_BEAM_INJECTION#283
  inject $GID 283 $1 4
  usleep 385000

  # CMD_START_ENERGY_RAMP#285
  inject $GID 285 $1 4 0x8
  usleep 422000
  
  # CMD_BEAM_EXTRACTION#284
  inject $GID 284 $1 4
  usleep 84000
  
  # CMD_START_ENERGY_RAMP#285
  inject $GID 285 $1 4 0x8
  usleep 1000000
  
  echo "end of sequnce" $1 `date`
}

# For gnuradio only
# configure_io_timing IO1

while true
do
  execute_sequnce3 3
  execute_sequnce7 7
  show_config
done

