#!/bin/sh

# exit on any error
set -e

function help {
    echo ""
    echo "Usage: reads the status of a fec <FEC_NAME>"
    echo ""
    echo "This script will read the status property of a FEC and return 0 if the status is ok"
    echo ""
    echo "-v verbose .. will as well print the uptime"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

if [ $# -gt 2 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi
if [ $# -lt 1 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
    exit 1
fi

FEC=$1
VERBOSE=FALSE
if [ ! -z "$2" ]; then
        VERBOSE=TRUE
fi

NOMEN=$(pdex -a $FEC | grep NOMEN | awk '{print $3}')

# If there are multiple devices, grap the first non global
if [ -z "$NOMEN" ]; then
        NOMEN=$(pdex -a $FEC | grep -v 'global' | grep -m 1 '|--' |  awk '{print $3}')
fi

#TRy int
if [ -z "$NOMEN" ]; then
        NOMEN=$(idex -a $FEC | grep NOMEN | awk '{print $3}')
fi

# If there are multiple devices, grap the first non global
if [ -z "$NOMEN" ]; then
        NOMEN=$(idex -a $FEC | grep -v 'global' | grep -m 1 '|--' |  awk '{print $3}')
fi

if [ -z "$NOMEN" ]; then
        echo "FEC not found error for FEC: $FEC" >&2
        exit 1
fi

STATUS=$(pdex -a $NOMEN Status | grep '| status' | awk '{print $9}')

#TRy int
if [ -z "$STATUS" ]; then
        STATUS=$(idex -a $NOMEN Status | grep '| status' | awk '{print $9}')
fi

if [ -z "$STATUS" ]; then
        echo "Failed to get status for FEC: $FEC" >&2
        exit 1
fi

if [ "$VERBOSE" = "TRUE" ]; then

  UPTIME=$(pdex -a $NOMEN SystemMonitor | grep '|-- uptime' | awk '{print $4}')

  echo -e "Status of $FEC ('$NOMEN'):\t$STATUS\tuptime: $UPTIME"
fi

if [ "$STATUS" = "OK" ]; then
  exit 0
else
  exit 1
fi