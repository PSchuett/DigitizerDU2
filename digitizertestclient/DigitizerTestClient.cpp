//RDA includes
#include <cmw-rda3/client/service/ClientServiceBuilder.h>
#include <cmw-rda3/common/config/Defs.h>

//misc includdes
#include <iostream>
#include <string>
#include <stdlib.h>     /* getenv */
#include <string.h> //strlen
#include <limits>
#include <math.h>

using namespace std;
using namespace cmw::util;
using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;

// Variables used in different tests
int64_t sample_count = 0;

int64_t first_digitizer = 0;
int64_t last_digitizer = 0;

double cycle_time_digitizer_first = 0;
double cycle_time_digitizer_last = 0;


// Variables for continous stamp test
int64_t last_measurement_local_time = 0;
size_t accumulated_samp_count = 0;
int64_t last_stamp = 0;
int64_t assert_distance_between_stamps = 0;
double expected_samp_rate = 0;
double samp_rate_tolerance_hz = 10;
int64_t continous_stamp_tolerance_ns = 1000; // Tolerance for 1MS ( samples to sample distance = 1µs )
int stamps_compared = 0;
int stamps_to_compare = 20000;

uint64_t get_timestamp_nano_utc()
{
    timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);
    return (start_time.tv_sec * 1000000000) + (start_time.tv_nsec);
}

void printException(const RdaException& ex)
{
    std::cout << "Exception caught:" << std::endl;
    cout << ex.what() << endl;
    std::vector<std::string> stack = ex.getStack();
    std::vector<std::string>::iterator iter;
    for ( iter = stack.begin(); iter != stack.end(); iter++ )
    {
        std::cout << *iter << std::endl;
    }
}

class ContinousStampsHandler: public NotificationListener
{
    public:

        virtual void dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> value, UpdateType updateType)
        {
            try
            {
                if( updateType == UT_NORMAL || updateType == UT_IMMEDIATE_UPDATE )
                {
                    if (value.get() == NULL)
                    {
                        std::string error = "ContinousStampsHandler: No Data available for this update";
                        cout << error << endl;
                        throw error;
                    }

                    //cout << value->getData().toString() << endl;
                   // cout << value->getContext().toString() << endl;

                    size_t size1, size2;
                    const int64_t refStamp = value->getData().getLong("refTriggerStamp");
                    const float* relativeTime = value->getData().getArrayFloat2D("channelTimeSinceRefTrigger", size1, size2);

                    if(size1 > 1)
                    {
                        cout << "No support for more than one channel at the same time .. .exiting" << std::endl;
                        exit(1);
                    }

                    accumulated_samp_count += size2;

                    uint64_t now = get_timestamp_nano_utc();
                    uint64_t first_sample_stamp = refStamp + int64_t(relativeTime[0] * 1000000000.);
                    cout << "## Reveived " << size2 <<  " samples " << ". Sample count: " << sample_count << "time diff: " << (now - first_sample_stamp) / 1000000. <<  "ms" << endl;
                    cout << "## Used Ref Trigger Stamp: " << refStamp << endl;
                    // ################ sample rate check ######################

                    if(accumulated_samp_count > 30000) // accumulate some samples to get more precise samp_rate
                    {
                        if( last_measurement_local_time != 0)
                        {
                            int64_t diff = now - last_measurement_local_time;
                            double calc_samp_rate = (1000000000. * accumulated_samp_count)/diff;
                            if( expected_samp_rate != 0 )
                            {
                                if (abs(expected_samp_rate - calc_samp_rate) > samp_rate_tolerance_hz) //If they differ more than 10Hz
                                {
                                    cout << "Error! Wrong sample rate. Expected: '"<< expected_samp_rate << "' Measured: '" << calc_samp_rate << "'." << std::endl;
                                    stamps_compared = 0;
                                }
                                else
                                {
                                    cout << "Sample Rate is fine. Expected: '"<< expected_samp_rate << "' Measured: '" << calc_samp_rate << "'." << std::endl;
                                }
                            }
                        }
                        last_measurement_local_time = now;
                        accumulated_samp_count = 0;
                    }

                    // ################ Check if stamps are continuous ######################
                    //cout << "size: " << size << endl;
                    for (size_t i = 0; i< size2; i++)
                    {
                        sample_count++;
                        int64_t new_stamp = refStamp + int64_t(relativeTime[i] * 1000000000.);
                        if( last_stamp != 0 )
                        {
                            int64_t diff = new_stamp - last_stamp;
                            if( assert_distance_between_stamps != 0)
                            {
                                //cout << diff / 1000. << "µs. Sample count: " << sample_count << endl;
                                if( abs(diff - assert_distance_between_stamps) > continous_stamp_tolerance_ns )
                                {
                                    if(i == 0)
                                        cout << "Error for sample " << sample_count << " (First sample of this update)"<< endl;
                                    else
                                        cout << "Error for sample " << sample_count << endl;
                                    cout << "Distance between stamps is     : " << round(diff / 1000.) << "µs. Sample count: " << sample_count << endl;
                                    cout << "assert_distance_between_stamps : " << round(assert_distance_between_stamps / 1000.) << "µs" << endl;
//                                    cout << "last_stamp      : " << last_stamp << endl;
//                                    cout << "new_stamp       : " << new_stamp << endl;
                                    //exit(1);
                                    stamps_compared = 0;
                                }
                            }

                            // Between different updates (i == 0), fluctuations are expected
                            // we dont want to use the fluctuations for self-calibration
                            if(i != 0)
                            {
                                assert_distance_between_stamps = diff;
                                expected_samp_rate = 1000000000./ assert_distance_between_stamps;
                            }

//                            cout << "Setting assert_distance_between_stamps to: " << assert_distance_between_stamps << endl;
//                            cout << "new_stamp: " << new_stamp << endl;
//                            cout << "last_stamp: " << last_stamp << endl;
                        }
                        //cout << "assert_distance_between_stamps: " << assert_distance_between_stamps << endl;
                        last_stamp = new_stamp;
                        stamps_compared ++;
                        if( stamps_compared == stamps_to_compare)
                        {
                            cout << "Test finished ok. "<< stamps_to_compare << " stamps compared." << endl;
                            cout << "- used stamp tolerance    : "<< continous_stamp_tolerance_ns <<" ns )" << endl;
                            cout << "- used samp Rate tolerance: "<< samp_rate_tolerance_hz <<" Hz )" << endl;
                            //exit(0);
                        }
                    }
//                    std::cout << "channelTimeSinceRefTrigger[start]: "  << relativeTime[0] << std::endl;
//                    std::cout << "channelTimeSinceRefTrigger[end]: "  << relativeTime[size - 1] << std::endl;
                }
                else if(updateType == UT_FIRST_UPDATE) //we dont care for the first-update of subscriptions
                    cout << "ContinousStampsHandler - UT_FIRST_UPDATE was ignored" << endl;
                else
                    cout << "ContinousStampsHandler - unknown update type" << endl;
            }
            catch (const std::exception& ex)
            {
                cout << ex.what() << endl;
            }
        }

        void errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType)
        {
            if( updateType != UT_FIRST_UPDATE )
            {
                cout << "error received in ContinousStampsHandler" << endl;
                cout << exception->what() << endl;
                cout << endl;
            }
        }
};

int main(int argc, char **argv)
{
 /*   <enum id="_171124080639_1" name="ACQUISITION_MODE">
        <item access="RW" id="_171124080639_2" symbol="STREAMING" value="0" />
        <item access="RW" id="_171124080639_3" symbol="FULL_SEQUENCE" value="1" />
        <item access="RW" id="_171124080639_4" symbol="SNAPSHOT" value="2" />
        <item access="RW" id="_171124080639_5" symbol="POST_MORTEM" value="3" />
        <item access="RW" id="_171124080639_6" symbol="TRIGGERED" value="4" />
    </enum>

        <enum id="_180829113228_0" name="STREAMING_CLIENT_UPDATE_FREQUENCY">
        <description>Rate which will be used to update a rda-client.</description>
        <item access="RW" id="_180829113236_0" symbol="FREQUENCY_1Hz" value="0" />
        <item access="RW" id="_180829113238_0" symbol="FREQUENCY_10Hz" value="1" />
        <item access="RW" id="_180829113238_1" symbol="FREQUENCY_25Hz" value="2" />
    </enum>
        */


    // If the client is outside the acc network, use FEC.acc.gsi.de as FECName
    // The correct server_name can be found on the top of the instance-file in the paragraph 'information'

    // ###########################################################
//    std::string server_name = "DigitizerDU2.dal007";
//    std::string device_name = "GSCD002";
//    std::string channel_name = "GSI11MU2:U_Set@10kHz";
    // ###########################################################
    std::string server_name = "DigitizerDU2.fel0060";
    std::string device_name = "GSCD053";
    std::string channel_name = "GS01KM3QS:Current@10kHz";
    // ###########################################################
//    std::string server_name = "DigitizerDU2.dal004";
//    std::string device_name = "FG2";
//    std::string channel_name = "GSI123XY4:Voltage_Rectangle@10kHz";
    // ###########################################################

    std::string prop_name = "AcquisitionDAQ";
    std::string cycle_name = "";
    //std::string cycle_name = "FAIR.SELECTOR.S=4294967295";

    // In order to subscribe to all cycles
    // std::string cycle_name = "";

    try
    {
        //cmwDirectoryList = getenv ("CMW_DIRECTORY_CLIENT_SERVERLIST");
        setenv ("CMW_DIRECTORY_CLIENT_SERVERLIST","cmwpro00a.acc.gsi.de:5021",true);
        //setenv ("CMW_DIRECTORY_CLIENT_SERVERLIST","cmwdev00a.acc.gsi.de:5021",true);
        int setenv(const char *envname, const char *envval, int overwrite);

        std::cout << "---------------------------------------------------------------"<< std::endl;
        //std::cout << "cmw-directory-server: " << cmwDirectoryList << std::endl;
        std::cout << "Server: " << server_name << std::endl;
        std::cout << "Device: " << device_name << std::endl;
        std::cout << "---------------------------------------------------------------"<< std::endl;

        auto_ptr<ClientService> clientService = ClientServiceBuilder::newInstance()->build();

        SubscriptionSharedPtr subscription;

        AccessPoint* accessPoint;
        NotificationListenerSharedPtr replyHandler;

        char input = ' ';
        while (input != '0')
        { //a very basic Menu

            std::cout << "---------------------------------------------------------------"<< std::endl;
            std::cout << "choose your action:" << std::endl;
            std::cout << "(1) Check if streaming timestamps are continuous (no stamps missing)" << std::endl;
            std::cout << "(0) exit" << std::endl;
            std::cout << "---------------------------------------------------------------"<< std::endl;
            std::cout << "# ";
            std::cin >> input;
            std::cout << "---------------------------------------------------------------"<< std::endl;

            switch (input)
            {
                case '1':
                {
                    replyHandler.reset(new ContinousStampsHandler);
                    accessPoint = &(clientService->getAccessPoint(device_name, prop_name ,server_name));
                    //NotificationListenerSharedPtr replyHandler(new PrintDataHandler);
                    auto_ptr<Data> filters = DataFactory::createData();
                    filters->append("channelNameFilter",channel_name.c_str());
                    filters->append("acquisitionModeFilter",0); // 0 = STREAMING
                    filters->append("clientUpdateFreuency",2); // 2 = FREQUENCY_25Hz
                    std::auto_ptr<RequestContext> context = RequestContextFactory::create(cycle_name, filters);
                    subscription = accessPoint->subscribe( context, replyHandler );
                    std::cout << "Subscribed to server: " << server_name << " Device: " << device_name << " Property: " << prop_name << std::endl;
                    break;
                }
//                case '4':
//                {
//                    if( subscription->getState() != Subscription::SS_SUBSCRIBED && subscription->getState() !=  Subscription::SS_DISCONNECTED)
//                    {
//                        std::cout << "There is no active subscription" << std::endl;
//                        break;
//                    }
//                    subscription->unsubscribe();
//                    break;
//                }
                case '0':
                    break;
                default:
                    std::cout << "Invalid Command" << std::endl;
            }

             //std::cout << "dcct_subscription:" <<  dcct_subscription->toString() << std::endl;

        } // end while
    }
    catch (const RdaException& ex)
    {
        printException(ex);
        return 1;
    }
    catch (const std::exception& ex)
    {
        cout << ex.what() << endl;
    }
    return 0;
}
