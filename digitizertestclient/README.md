# DigitizerTestClient

Client in order to test some parameters of a FESA Digitizer class

It is majorly used to validate that streaming timestamps are continuous (no stamps are missing)

# Build and Run

Modify Device/Property/Channel-names in the source code according to your needs

```
make clean
make
./DigitizerTestClient
```
